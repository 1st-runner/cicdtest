FROM loc:5000/openjdk:17-alpine
COPY app.war App1.war
ENTRYPOINT ["java","-jar","/App1.war"]
