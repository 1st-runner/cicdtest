package com.example.lololol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LolololApplication {

	public static void main(String[] args) {
		SpringApplication.run(LolololApplication.class, args);
	}

}
